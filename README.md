# OpenML dataset: Swiss-banknote-conterfeit-detection

https://www.openml.org/d/43477

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
Will you be able to identify genuine and conterfeit banknotes, even if half of the data is conterfeit? Perfect for testing different outlier detection algorithms.
Content
The dataset includes information about the shape of the bill, as well as the label. It is made up of 200 banknotes in total, 100 for genuine/conterfeit each.
Attributes:
-conterfeit: Wether a banknote is conterfeit (1) or genuine (0)
-Length: Length of bill (mm)
-Left: Width of left edge (mm)
-Right: Width of right edge (mm)
-Bottom: Bottom margin width (mm)
-Top: Top margin width (mm)
-Diagonal: Length of diagonal (mm)
Original Data Source
Flury, B. and Riedwyl, H. (1988). Multivariate Statistics: A practical approach. London: Chapman  Hall, Tables 1.1 and 1.2, pp. 5-8.
Applications
While it might be pretty easy for a classifier to decide wether the banknotes are conterfeit or not, what about methods using outlier detection?
Classical methods of outlier detection won't work, since half of the data consist of outliers (conterfeit bills), so more robust methods will be needed.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43477) of an [OpenML dataset](https://www.openml.org/d/43477). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43477/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43477/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43477/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

